package domain;

public class Category {
    private long id;
    private String name_rus;
    private String name_kaz;
    private String name_eng;
    private Category parent_id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName_rus() {
        return name_rus;
    }

    public void setName_rus(String name_rus) {
        this.name_rus = name_rus;
    }

    public String getName_kaz() {
        return name_kaz;
    }

    public void setName_kaz(String name_kaz) {
        this.name_kaz = name_kaz;
    }

    public String getName_eng() {
        return name_eng;
    }

    public void setName_eng(String name_eng) {
        this.name_eng = name_eng;
    }

    public Category getParent_id() {
        return parent_id;
    }

    public void setParent_id(Category parent_id) {
        this.parent_id = parent_id;
    }

    @Override
    public String toString() {
        return "Category{" + "id=" + id
                + ", name_rus" + name_rus
                + ", name_kaz" + name_kaz
                + ", name_eng" + name_eng
                + ", parent_id" + parent_id
                + "}";
    }

}
package domain;

public class Question {
    private long id;
    private String question_rus;
    private String question_kaz;
    private String question_eng;
    private String answer_rus;
    private String answer_kaz;
    private String answer_eng;
    private Category category_id;
    private long counter;

    public Question(String question_rus, String answer_rus) {
        setQuestion_rus(question_rus);
        setAnswer_rus(answer_rus);
    }


    public Question(long id, String question_rus, String question_kaz, String question_eng, String answer_rus, String answer_kaz, String answer_eng) {
        setId(id);
        setQuestion_rus(question_rus);
        setQuestion_kaz(question_kaz);
        setQuestion_eng(question_eng);
        setQuestion_rus(answer_rus);
        setAnswer_kaz(question_kaz);
        setQuestion_eng(answer_eng);

    }

    public Question(long id,String question_kaz, String answer_kaz) {
        setId(id);
        setQuestion_kaz(question_kaz);
        setAnswer_kaz(answer_kaz);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getQuestion_rus() {
        return question_rus;
    }

    public void setQuestion_rus(String question_rus) {
        this.question_rus = question_rus;
    }

    public String getQuestion_kaz() {
        return question_kaz;
    }

    public void setQuestion_kaz(String question_kaz) {
        this.question_kaz = question_kaz;
    }

    public String getQuestion_eng() {
        return question_eng;
    }

    public void setQuestion_eng(String question_eng) {
        this.question_eng = question_eng;
    }

    public String getAnswer_rus() {
        return answer_rus;
    }

    public void setAnswer_rus(String answer_rus) {
        this.answer_rus = answer_rus;
    }

    public String getAnswer_kaz() {
        return answer_kaz;
    }

    public void setAnswer_kaz(String answer_kaz) {
        this.answer_kaz = answer_kaz;
    }

    public String getAnswer_eng() {
        return answer_eng;
    }

    public void setAnswer_eng(String answer_eng) {
        this.answer_eng = answer_eng;
    }

    public Category getCategory_id() {
        return category_id;
    }

    public void setCategory_id(Category category_id) {
        this.category_id = category_id;
    }

    public long getCounter() {
        return counter;
    }

    public void setCounter(long counter) {
        this.counter = counter;
    }

    @Override
    public String toString() {
        return "questionRus:  " + question_rus + "\n" +
                "answerRus:  " + answer_rus;
    }

}

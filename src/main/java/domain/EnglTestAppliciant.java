package domain;

import java.sql.Date;

public class EnglTestAppliciant {
    private String email;
    private String name;
    private String surname;
    private long IIN;
    private long phone;
    private Date date;

    public EnglTestAppliciant(String email, String name, String surname, long IIN, long phone, Date date) {
        this.email = email;
        this.name = name;
        this.surname = surname;
        this.IIN = IIN;
        this.phone = phone;
        this.date = date;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public long getIIN() {
        return IIN;
    }

    public void setIIN(long IIN) {
        this.IIN = IIN;
    }

    public long getPhone() {
        return phone;
    }

    public void setPhone(long phone) {
        this.phone = phone;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "EnglTestAppliciant {" + "email :" + email
                + ", name" + name
                + ", surname" + surname
                + ", IIN" + IIN
                + ", phone" + phone
                + ",day" + date +
                "}";
    }
}

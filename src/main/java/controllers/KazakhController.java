package controllers;

import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import utill.InlineButtonUtill;

public class KazakhController {

    SendMessage sendMessage = new SendMessage();

    public SendMessage kazDegrees(String text, Long chatId, Integer messageId) {
        sendMessage.setChatId(chatId);

        if (text.equals("Мен абитуриентпін")) {
            sendMessage.setText("Cіз қандай ғылыми дәрежені алғыныз келеді?\n" +
                    "1.Бакалаврлық \n" +
                    "2.Магистрлық ");
            sendMessage = sendMessage.disableWebPagePreview();
        } else if (text.equals("Бакалаврлық")) {
            sendMessage.setText(("Не жәйлі білгіңіз келеді?"));

            sendMessage.setReplyMarkup(InlineButtonUtill.keyboardMarkup(
                    InlineButtonUtill.collection(
                            InlineButtonUtill.rows(InlineButtonUtill.button("Бакалавриат білім беру бағдарламалары", "bdKazProg"),
                                    InlineButtonUtill.button("Университет жәйлі", "universityKaz")),
                            InlineButtonUtill.rows(InlineButtonUtill.button("Қабылдау комиссиясы", "bKazAdmissions"))
                    )));
        } else if (text.equals("Магистрлық")) {
            sendMessage.setReplyMarkup(InlineButtonUtill.keyboardMarkup(
                    InlineButtonUtill.collection(
                            InlineButtonUtill.rows(InlineButtonUtill.button(" Магистратура білім беру бағдарламалары", "mdKazProg"),
                                    InlineButtonUtill.button("Университет жәйлі", "universityKaz")),
                            InlineButtonUtill.rows(InlineButtonUtill.button("Қабылдау комиссиясы", "mKazAdmissions"))
                    )));
        }else if (text.equals("Басқа компаниялармен ынтымақтастық") || text.equals("Ынтымақтастық")) {
            sendMessage.setText("Біз мынадай ірі масштабты компаниялармен ынтымақтасамыз:\n" +
                    "“Qazaq Cybersport Federation” және “Academy Esports”\n" +
                    "Astana IT University, «Qazaq Cybersport Federation» республикалық қоғамдық бірлестігі және «Academy Esports » ЖШС арасындағы 2019 жылғы 3 қыркүйектегі өзара түсіністік пен ынтымақтастық туралы үшжақты меморандум аясында киберспорт бойынша республикалық ауқымдағы турнир өткізу жоспарлануда.\n" +
                    "“Qaztech Ventures”\n" +
                    "Astana IT University мен «Qaztech Ventures» АҚ арасындағы 2019 жылғы 6 қыркүйектегі өзара түсіністік және ынтымақтастық туралы меморандум аясында технологиялық кәсіпкерлік пен инновацияны дамыту бойынша ақпарат пен тәжірибе алмасу үшін хакатондар, форумдар, конкурстар мен мастер-класстар өткізу жоспарланған. Сонымен қатар, әрі қарай коммерциаландыру мақсатында бірлесе отырып технологиялық шешімдер базасын құру және университет студенттерінің тағылымдамадан өтулеріне көмек көрсетуі қарастырылуда.\n" +
                    "HP компаниясының филиалы\n" +
                    "Astana IT University мен HP филиалы арасындағы 2019 жылғы 11 қыркүйектегі өзара түсіністік және ынтымақтастық туралы меморандум аясында HP университеттің аудиториялары мен зертханаларын HP өнімдерімен жабдықтау және студенттер мен жұмысшылардың НР ноутбуктерін жеңілдікті шарттармен сатып алу мүмкіндігін қарастырылуда. HP компаниясы университет ұйымдастырған eSports республикалық турнирінің демеушілерінің бірі болады.\n" +
                    "«Асбис Қазақстан» ЖШС\n" +
                    "Astana IT University мен «Асбис Қазақстан» ЖШС арасындағы 2019 жылғы 13 қыркүйектегі өзара түсіністік және ынтымақтастық туралы меморандум аясында университет студенттері мен қызметкерлерінің Apple өнімдерін тиімді шарттармен сатып алуы қарастырылуда.\n" +
                    "Kolesa Group\n" +
                    "Astana IT University мен Kolesa Group арасындағы 2019 жылғы 13 қыркүйектегі өзара түсіністік пен ынтымақтастық туралы меморандум аясында Data Science және Machine Learning бойынша мастер-класстар мен дәрістер өткізу жоспарлануда. Сондай-ақ, Astana IT University студенттерінің Kolesa Group компаниясында ақылы тағылымдамадан өту мүмкіндігі қарастырылуда.\n" +
                    "Lenovo Kazakhstan\n" +
                    "Astana IT University мен Lenovo Kazakhstan арасындағы 2019 жылғы 3 қазандағы өзара түсіністік және ынтымақтастық туралы меморандум аясында университет ұйымдастыратын ірі іс-шараларға демеушілік көмек көрсету және студенттер мен қызметкерлерге тиімді шарттармен жабдықтар алу мүмкіндігі қарастырылуда.\n" +
                    "Chocofamily Group\n" +
                    "Astana IT University мен Chocofamily Group арасындағы 2019 жылғы 4 қазандағы өзара түсіністік және ынтымақтастық туралы меморандум аясында студенттердің ақылы тағылымдамадан өту мүмкіндігі қарастырылуда. Сонымен қатар бірлесе отырып хакатондар, дәрістер мен мастер-класстар өткізу жоспарлануда.\n" +
                    "KPMG\n" +
                    "Astana IT University мен KPMG арасындағы 2019 жылғы 4 қазандағы өзара түсіністік және ынтымақтастық туралы меморандум аясында бірлескен хакатондар, дәрістер мен мастер-класстар өткізу жоспарлануда. Сондай-ақ, ғылыми-зерттеу бағытындағы ынтымақтастық, бірлескен магистратура мен PhD докторантура бағдарламаларын ашу мүмкіндігі, оның аясында KPMG үшін мамандар даярлау және студенттердің тағылымдамадан өтіп, кейіннен жұмысқа орналасуы қарастырылады.\n" +
                    "Senim Group of Companies\n" +
                    "Astana IT University мен Senim Group of Companies арасындағы 2019 жылғы 22 қазандағы өзара түсіністік және ынтымақтастық туралы меморандум аясында бірлескен хакатондар өткізу және студенттердің стартап жобаларын қолдау жоспарлануда. Сонымен қатар студенттердің тағылымдамадан өтуі және компанияда одан әрі жұмысқа орналасу мүмкіндігі қарастырылуда.\n" +
                    "EPAM Kazakhstan\n" +
                    "Astana IT University мен EPAM Kazakhstan арасындағы 2019 жылғы 30 қазандағы өзара түсіністік және ынтымақтастық туралы меморандум аясында студенттердің ақылы тағылымдамадан өту мүмкіндігі қарастырылуда. Сонымен қатар бірлесе отырып хакатондар, дәрістер мен мастер-класстар өткізу жоспарлануда." );
            sendMessage = sendMessage.disableWebPagePreview();

        } else if (text.equals("Байланысу")) {
            sendMessage.setText("Университет мекен-жайы:\n" +
                    "Нур-СултанБизнес-центр EXPO, блок C.1.\n" +
                    "Казахстан, 010000.\n" +
                    "Телефон нөміріміз: +7 (7172)645-710 .\n" +
                    "Почта:info@astanait.edu.kz");
            sendMessage = sendMessage.disableWebPagePreview();

        }else if(text.equals("Тур") ||text.equals("3D тур") ||text.equals("Увидеть университет") ){
            sendMessage.setText("Егер сіз университетті кӨргіңіз келсе, онда [3D тур](https://3dtour.expo2017astana.com/index.html?s=pano415335)  өтуіңізге болады");
            sendMessage.setParseMode("Markdown");
            sendMessage = sendMessage.disableWebPagePreview();

        }

        return sendMessage;
    }

    public SendMessage kazStudents(String text, Long chatId, Integer messageId) {
        sendMessage.setChatId(chatId);
        if (text.equals("Мен студентпін")) {

        }
        return sendMessage;
    }

}

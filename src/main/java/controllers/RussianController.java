package controllers;

import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import utill.InlineButtonUtill;

public class RussianController {

    SendMessage sendMessage = new SendMessage();

    public SendMessage rusDegrees(String text, Long chatId, Integer messageId) {
        sendMessage.setChatId(chatId);

        if (text.equals("Я абитуриент")) {
            sendMessage.setText("Куда вы хотите поступить?\n" +
                    "1.Бакалавриат \n" +
                    "2.Магистратура");
            sendMessage = sendMessage.disableWebPagePreview();
        } else if (text.equals("Бакалавриат")) {
            sendMessage.setText(("Что вас интересует?"));

            sendMessage.setReplyMarkup(InlineButtonUtill.keyboardMarkup(
                    InlineButtonUtill.collection(
                            InlineButtonUtill.rows(InlineButtonUtill.button("Образовательные программы бакалавриата", "bdRusProg"),
                                    InlineButtonUtill.button("Сам университет", "universityRus")),
                            InlineButtonUtill.rows(InlineButtonUtill.button("Приемная комиссия", "bRusAdmissions"))
                    )));
        } else if (text.equals("Магистратура")) {
            sendMessage.setText(("Что вас интересует?"));

            sendMessage.setReplyMarkup(InlineButtonUtill.keyboardMarkup(
                    InlineButtonUtill.collection(
                            InlineButtonUtill.rows(InlineButtonUtill.button("Образовательные программы магистратуры", "mdRusProg"),
                                    InlineButtonUtill.button("Сам университет", "universityRus")),
                            InlineButtonUtill.rows(InlineButtonUtill.button("Приемная комиссия", "mRusAdmissions"))
                    )));
        } else if (text.equals("Сотрудничество с другими компаниями") || text.equals("Сотрудничество")) {
            sendMessage.setText("Мы сотрудничам с такими крупномасштабными компаниями как:\n" +
                    "РОО “Qazaq Cybersport Federation” и ТОО “Academy Esports”\n" +
                    "В рамках трехстороннего меморандума о взаимопонимании и сотрудничестве от 3 сентября 2019 года, подписанного между Astana IT University, РОО “Qazaq Cybersport Federation” и ТОО “Academy Esports”, планируется проведение совместного турнира по киберспорту Республиканского масштаба.\n" +
                    "АО “Qaztech Ventures”\n" +
                    "В рамках Меморандума о взаимопонимании и сотрудничестве от 6 сентября 2019 года, подписанного между Astana IT University и АО “Qaztech Ventures” планируется проведение хакатонов, форумов, конкурсов и мастер-классов для обмена информацией и опытом по вопросам развития технологического предпринимательства и инноваций. А также рассматривается совместное создание базы технологических решений, с целью их дальнейшей коммерциализации и содействие прохождению студентами университета стажировок в Обществе.\n" +
                    "Филиал компании HP\n" +
                    "В рамках Меморандума о взаимопонимании и сотрудничестве от 11 сентября 2019 года, подписанного между Astana IT University и Филиал компании HP рассматривается возможность оснащения кабинетов, аудиторий и лабораторий Университета продукцией HP и приобретения продукции HP студентами и сотрудникам на выгодных условиях. Компания HP выступит одним из спонсоров на республиканском турнире по киберспорту организуемый Университетом.\n" +
                    "ТОО “Асбис Казахстан”\n" +
                    "В рамках Меморандума о взаимопонимании и сотрудничестве от 13 сентября 2019 года, подписанного между Astana IT University и ТОО “Асбис Казахстан”, рассматривается приобретение продукции Apple студентами и сотрудникам на выгодных условиях.\n" +
                    "Kolesa Group\n" +
                    "В рамках Меморандума о взаимопонимании и сотрудничестве от 13 сентября 2019 года, подписанного между Astana IT University и Kolesa Group планируется проведение мастер-классов, лекций по Data Science и Machine Learning. А также рассматривается возможность прохождения оплачиваемых стажировок в Kolesa Group студентами Astana IT University.\n" +
                    "Lenovo Kazakhstan\n" +
                    "В рамках Меморандума о взаимопонимании и сотрудничестве от 3 октября 2019 года, подписанного между Astana IT University и Lenovo Kazakhstan, рассматривается возможность оказания спонсорской помощи для крупных, мероприятий организуемые университетом, и приобретения оборудования студентами и сотрудникам на выгодных условиях.\n" +
                    "Chocofamily Group\n" +
                    "В рамках Меморандума о взаимопонимании и сотрудничестве от 4 октября 2019 года, подписанного между Astana IT University и Chocofamily Group, рассматривается возможность прохождения студентами платных стажировок. А также планируется проведение Совместных хакатонов, гостевых лекций, мастер-классов на базе Университета.\n" +
                    "KPMG\n" +
                    "В рамках Меморандума о взаимопонимании и сотрудничестве от 10 октября 2019 года, подписанного между Astana IT University и KPMG, планируется проведение совместных хакатонов, лекций и мастер-классов. А также, рассматривается возможность сотрудничества в научно-исследовательском направлении, открытие совместных программ магистратуры и PhD докторантуры, в рамках которого планируется готовить специалистов для KMPG и прохождение стажировки студентами с последующим трудоустройством.\n" +
                    "Senim Group of Companies\n" +
                    "В рамках Меморандума о взаимопонимании и сотрудничестве от 22 октября 2019 года, подписанного между Astana IT University и Senim Group of Companies, планируется проведение совместных хакатонов и поддержка стартап проектов студентов. А также рассматривается возможность прохождения стажировок студентами и дальнейшее трудоустройство в компании.\n" +
                    "EPAM Kazakhstan\n" +
                    "В рамках Меморандума о взаимопонимании и сотрудничестве от 30 октября 2019 года, подписанного между Astana IT University и EPAM Kazakhstan, рассматривается возможность прохождения студентами платных стажировок. А также планируется проведение Совместных хакатонов, гостевых лекций, мастер-классов.");
            sendMessage = sendMessage.disableWebPagePreview();

        } else if (text.equals("Связаться")) {
            sendMessage.setText("Университет находится по адресу:\n" +
                    "Нур-Султан\n" +
                    "Бизнес-центр EXPO, блок C.1.\n" +
                    "Казахстан, 010000.\n" +
                    "Номер телефона: +7 (7172)645-710 .\n" +
                    "Почта:info@astanait.edu.kz");

            sendMessage = sendMessage.disableWebPagePreview();

        } else if (text.equals("3D тур") || text.equals("Увидеть университет")) {
            sendMessage.setText("Если вы хотите увидеть наш университет, то можете пройти [3D тур](https://3dtour.expo2017astana.com/index.html?s=pano415335)");
            sendMessage.setParseMode("Markdown");
            sendMessage = sendMessage.disableWebPagePreview();

        }

        return sendMessage;
    }

    public SendMessage rusStudents(String text, Long chatId, Integer messageId) {
        sendMessage.setChatId(chatId);
        if (text.equals("Я студент")) {
            sendMessage.setText(("Что надо?"));

            sendMessage.setReplyMarkup(InlineButtonUtill.keyboardMarkup(
                    InlineButtonUtill.collection(
                            InlineButtonUtill.rows(InlineButtonUtill.button("Общага", "dorm"),
                                    InlineButtonUtill.button("Военка", "militaryFaculty"),
                                    InlineButtonUtill.button("МЕДИКЕР", "clinic")),
                            InlineButtonUtill.rows(InlineButtonUtill.button("Клубы", "clubs"),
                                    InlineButtonUtill.button("Центр психологического консультирования", "psychCounseling"))
                    )));
        }

        return sendMessage;
    }

}

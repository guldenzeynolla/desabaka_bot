package controllers;

import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import utill.InlineButtonUtill;

public class EnglishController {
    SendMessage sendMessage = new SendMessage();


    public SendMessage englDegrees(String text, Long chatId, Integer messageId) {
        sendMessage.setChatId(chatId);

        if (text.equals("I'm an applicant") || text.equals("I am an applicant")) {
            sendMessage.setText("What degree do you apply for?\n" +
                    "1.Bachelor's degree \n" +
                    "2.Master's degree");
            sendMessage = sendMessage.disableWebPagePreview();
        } else if (text.equals("1") || text.equals("Bachelor's degree")) {
            sendMessage.setText(("What are you interested in?"));

            sendMessage.setReplyMarkup(InlineButtonUtill.keyboardMarkup(
                    InlineButtonUtill.collection(
                            InlineButtonUtill.rows(InlineButtonUtill.button("Bachelor's degree programs", "bdProg"),
                                    InlineButtonUtill.button("University itself", "universityEngl")),
                            InlineButtonUtill.rows(InlineButtonUtill.button("University admissions Committee", "bAdmissions"))
                    )));
        } else if (text.equals("2") || text.equals("Master's degree")) {
            sendMessage.setReplyMarkup(InlineButtonUtill.keyboardMarkup(
                    InlineButtonUtill.collection(
                            InlineButtonUtill.rows(InlineButtonUtill.button(" Master's degree programs", "mdEnglProg"),
                                    InlineButtonUtill.button("University itself", "universityEngl")),
                            InlineButtonUtill.rows(InlineButtonUtill.button("University admissions Committee", "mAdmissions"))
                    )));
        } else if (text.equals("Сотрудничество с другими компаниями") || text.equals("Сотрудничество")) {
            sendMessage.setText("Мы сотрудничам с такими крупномасштабными компаниями как:\n" +
                    "“Qazaq Cybersport Federation” and “Academy Esports”\n" +
                    "As part of the tripartite Memorandum of Understanding and Cooperation dated as of September 3, 2019, signed between Astana IT University, ROO Qazaq Cybersport Federation, and Academy Esports LLP, a joint Republican-level e-sports tournament is planned.\n" +
                    "“Qaztech Ventures”\n" +
                    "As part of the Memorandum of Understanding and Cooperation dated as of September 6, 2019, signed between Astana IT University and Qaztech Ventures JSC, it is planned to hold hackathons, forums, competitions and master classes to exchange information and experience on the development of technological entrepreneurship and innovation. It also considers the joint creation of a base of technological solutions in order to further commercialize them and facilitate the passage of internships by university students in the Company.\n" +
                    "HP\n" +
                    "As part of the Memorandum of Understanding and Cooperation dated as of September 6, 2019, signed between Astana IT University and the HP Branch, the possibility of equipping classrooms and laboratories of the University with HP products and the acquisition of HP laptops by students and employees on favorable terms is being considered. HP will be one of the sponsors of the Republican eSports Tournament organized by the University.\n" +
                    "Asbis Kazakhstan LLP\n" +
                    "As part of the Memorandum of Understanding and Cooperation dated as of September 13, 2019, signed between Astana IT University and Asbis Kazakhstan LLP, the possibility of aquiring Apple products by students and employees on favorable terms is being considered.\n" +
                    "Kolesa Group\n" +
                    "As part of the Memorandum of Understanding and Cooperation dated as of September 13, 2019, signed between Astana IT University and the Kolesa Group, it is planned to conduct master classes, lectures on Data Science and Machine Learning. It also considers the possibility of passing paid internships in Kolesa Group by students of Astana IT University.\n" +
                    "Lenovo Kazakhstan\n" +
                    "As part of the Memorandum of Understanding and Cooperation dated as of October 3, 2019, signed between Astana IT University and Lenovo Kazakhstan, the possibility of providing sponsorship for large events organized by the university and the acquisition of equipment by students and staff on favorable terms is being considered.\n" +
                    "Chocofamily Group\n" +
                    "As part of the Memorandum of Understanding and Cooperation dated as of October 4, 2019, signed between Astana IT University and Chocofamily Group, the possibility of passing paid internships by students is being considered. It is also planned to hold Joint Hackathons, guest lectures, and master classes at the University.\n" +
                    "KPMG\n" +
                    "As part of the Memorandum of Understanding and Cooperation of October 10, 2019, signed between Astana IT University and KPMG, it is planned to hold joint hackathons, lectures and master classes. Moreover, the possibility of cooperation in the research, the opening of joint master's PhD programs, within the framework of which it is planned to train specialists for KMPG and internships by students with subsequent employment is being considered.\n" +
                    "Senim Group of Companies\n" +
                    "As part of the framework of the Memorandum of Understanding and Cooperation dated as of October 22, 2019, signed between Astana IT University and the Senim Group of Companies, it is planned to hold joint hackathons and support start-up projects of students. The possibility of internships by students and further employment in the company is also considered.\n" +
                    "EPAM Kazakhstan\n" +
                    "As part of the Memorandum of Understanding and Cooperation dated as of October 30, 2019, signed between Astana IT University and EPAM Kazakhstan, the possibility of passing paid internships by students is being considered. It is also planned to hold Joint hackathons, guest lectures, and master classes.");
            sendMessage = sendMessage.disableWebPagePreview();

        } else if (text.equals("Contact")) {
            sendMessage.setText("The University is located at:\n" +
                    "Nur-Sultan\n" +
                    "EXPO business center, block C. 1.\n" +
                    "Kazakhstan, 010000.\n" +
                    "Phone number: +7 (7172)645-710 .\n" +
                    "Mail:info@astanait.edu.kz");

            sendMessage = sendMessage.disableWebPagePreview();

        } else if (text.equals("Tour") || text.equals("3D tour") || text.equals("See the University")) {
            sendMessage.setText("If you want to see our University, you can take [3D tour](https://3dtour.expo2017astana.com/index.html?s=pano415335)");
            sendMessage.setParseMode("Markdown");
            sendMessage = sendMessage.disableWebPagePreview();

        }

        return sendMessage;
    }

    public SendMessage englStudents(String text, Long chatId, Integer messageId) {
        sendMessage.setChatId(chatId);
        if (text.equals("I'm a student") || text.equals("I am a student")) {
        }
        return sendMessage;
    }

}
package controllers;

import domain.Question;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import services.CommandService;
import services.interfaces.ICommandService;

import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class DesBot extends TelegramLongPollingBot {// -бот реализует метод получения апдейта через гет апдейт
    private ICommandService commandService = new CommandService();

    public static final String TOKEN = "1280330672:AAGuOLZpqmSO7q6KQwvT3KTqTeVa752x3rs";
    public static final String USERNAME = "Desabaka_bot";

    private CommandsController commandsController;
    private EnglishController englishController;
    private RussianController russianController;
    private KazakhController kazakhController;

    public DesBot() {
        this.commandsController = new CommandsController();
        this.englishController = new EnglishController();
        this.russianController = new RussianController();
        this.kazakhController = new KazakhController();
    }

    @Override
    public void onUpdateReceived(Update update) {
        SendMessage sendMessage = new SendMessage();

        if (update.getMessage().getText().equals("/registration")){
                sendMessage.setText("Write your email:");

                Message email = ;

        }
        if (update.hasCallbackQuery()) {
            CallbackQuery callbackQuery = update.getCallbackQuery();

            String data = callbackQuery.getData();
            User user = callbackQuery.getFrom();
            Message message = callbackQuery.getMessage();


            if (data.equals("english")) {
                sendMessage.setText(Stream.of("Hello", user.getLastName(), user.getFirstName(), ", I am a Telegram bot of Astana IT University, and you?(Please write down who you are)\n" +
                        "1.I am a student\n" +
                        "2.I am an applicant")
                        .filter(Objects::nonNull)//https://habr.com/ru/post/302628/
                        .collect(Collectors.joining(" ")));//крч без этого не получится
            } else if (data.equals("russian")) {
                sendMessage.setText(Stream.of("Привет", user.getLastName(), user.getFirstName(), ", я телеграмм бот Astana IT University,а ты?(Пожалуйста,напиши кто ты)\n" +
                        "1.Я студент\n" +
                        "2.Я абитуриент")
                        .filter(Objects::nonNull)//https://habr.com/ru/post/302628/
                        .collect(Collectors.joining(" ")));//крч без этого не получится
            } else if (data.equals("kazakh")) {
                sendMessage.setText(Stream.of("Cәлем", user.getLastName(), user.getFirstName(), ", мен Astana IT University телеграмм ботымын,ал сен кімсін?(Өтінем, кім екеніңді жазшы)\n" +
                        "1.Мен студентпін\n" +
                        "2.Мен абитуриентпін")
                        .filter(Objects::nonNull)//https://habr.com/ru/post/302628/
                        .collect(Collectors.joining(" ")));//крч без этого не получится
            } else if (data.equals("bdRusProg")) {
                sendMessage.setText("В нашем университете 9 образовательных программ бакалавриата,это:\n" +
                        "1.[Компьютерные науки](https://astanait.edu.kz/computer-science/)\n" +
                        "2.[Программная инженерия](https://astanait.edu.kz/software-engineering/)\n" +
                        "3.[Анализ больших данных](https://astanait.edu.kz/big-data-analysis/)\n" +
                        "4.[Промышленная автоматизация](https://astanait.edu.kz/industrial-automation/)\n" +
                        "5.[Медиа технологии](https://astanait.edu.kz/media-technologies/)\n" +
                        "6.[Кибербезопасность](https://astanait.edu.kz/cybersecurity/)\n" +
                        "7.[Телекоммуникационные системы](https://astanait.edu.kz/telecommunication-systems/)\n" +
                        "8.[IT Менеджмент](https://astanait.edu.kz/it-management/)\n" +
                        "9.[Цифровая журналистика](https://astanait.edu.kz/digital-journalism/)\n");
                sendMessage.setParseMode("Markdown");
            } else if (data.equals("bdKazProg")) {
                sendMessage.setText("Біздің университетте бакалавриаттың 9 білім беру бағдарламасы бар, бұл:\n" +
                        "1.[Компьютер ғылымдары](https://astanait.edu.kz/computer-science-3/)\n" +
                        "2.[Бағдарламалық инженерия](https://astanait.edu.kz/software-engineering-3/)\n" +
                        "3.[Үлкен деректерді талдау](https://astanait.edu.kz/big-data-analysis/-3)\n" +
                        "4.[Өнеркәсіптік автоматтандыру](https://astanait.edu.kz/industrial-automation-3/)\n" +
                        "5.[Медиатехнологиялар](https://astanait.edu.kz/media-technologies-3/)\n" +
                        "6.[Киберқауіпсіздік](https://astanait.edu.kz/cybersecurity-3/)\n" +
                        "7.[Телекоммуникациялық жүйелер](https://astanait.edu.kz/telecommunication-systems-3/)\n" +
                        "8.[IT Менеджмент](https://astanait.edu.kz/it-management-3/)\n" +
                        "9.[Сандық журналистика](https://astanait.edu.kz/digital-journalism-3/)\n");
                sendMessage.setParseMode("Markdown");
            } else if (data.equals("bdEnglProg")) {
                sendMessage.setText("Our University has 9 bachelor's degree programs, which are:\n" +
                        "1.[Computer Science](https://astanait.edu.kz/computer-science-2/)\n" +
                        "2.[Software Engineering](https://astanait.edu.kz/software-engineering-2/)\n" +
                        "3.[Big data Analysis](https://astanait.edu.kz/big-data-analysis-2/)\n" +
                        "4.[Industrial Automation](https://astanait.edu.kz/industrial-automation-2/)\n" +
                        "5.[Media Technologies](https://astanait.edu.kz/media-technologies-2/)\n" +
                        "6.[Cybersecurity](https://astanait.edu.kz/cybersecurity-2/)\n" +
                        "7.[Telecommunication Systems](https://astanait.edu.kz/telecommunication-systems-2/)\n" +
                        "8.[IT Management](https://astanait.edu.kz/it-management-2/)\n" +
                        "9.[Digital Journalism](https://astanait.edu.kz/digital-journalism-2/)\n");
                sendMessage.setParseMode("Markdown");
            } else if (data.equals("universityRus")) {
                sendMessage.setText("Astana IT University – ведущий центр компетенций по цифровой трансформации в Центральной Азии.\n" +
                        "1.Сотрудничество с другими компаниями\n" +
                        "2.Связаться\n" +
                        "3.3D тур\n");
            } else if (data.equals("universityKaz")) {
                sendMessage.setText("Astana IT University-Орталық Азиядағы сандық трансформация бойынша жетекші құзыреттілік орталығы.\n" +
                        "1.Басқа компаниялармен ынтымақтастық\n" +
                        "2.Байланыс\n" +
                        "3.3D тур");
            } else if (data.equals("universityEngl")) {
                sendMessage.setText("Astana IT University is a leading competence center for digital transformation in Central Asia.\n" +
                        "1. Cooperation with other companies\n" +
                        "2. Contact\n" +
                        "3. 3D tour");
            } else if (data.equals("mdRusProg")) {
                sendMessage.setText("В нашем университете единственная образовательная программа магистратуры,это:Управление проектами.\n" +
                        "Управление проектами — это дисциплина с постоянно растущим спросом как в частном так и в государственном секторе. Deutsche Bank Research заявляет, что к 2020 году проектная экономическая активность будет составлять 15% от создания стоимости в Германии. С другой стороны, в «Глобальном отчете по управлению проектами», опубликованном в 2012 году Price Water House, указывалось, что в течение периода 2000–2020 годов более 25% экономической ценности будет приходиться на проекты. Поэтому на рынке будет высокий спрос на квалифицированного специалиста, специализирующегося в управлении проектами.\n" +
                        "\n" +
                        "В ответ на это требование, Астана IT University при поддержке KPMA подготовит руководителей проектов с передовыми навыками, которые будут управлять сложными проектами в различных отраслях.[Узнать больше](https://astanait.edu.kz/masters-programs/)");
                sendMessage.setParseMode("Markdown");
            } else if (data.equals("mdKazProg")) {
                sendMessage.setText("В нашем университете единственная образовательная программа бакалавриата,это:Жобаларды басқару.\n" +
                        "Жобаларды басқару-бұл жеке және мемлекеттік секторда үнемі өсіп келе жатқан сұраныс. Deutsche Bank Research 2020 жылға қарай жобалық экономикалық белсенділік Германияда құнын құрудың 15% - ын құрайтынын мәлімдейді. Екінші жағынан, 2012 жылы Price Water House жарияланған \"жобаларды басқару жөніндегі жаһандық есепте\"2000-2020 жылдар кезеңінде экономикалық құндылықтың 25% - дан астамы жобаларға тиесілі болады деп көрсетілген. Сондықтан нарықта жобаларды басқаруға маманданған білікті маманға сұраныс жоғары болады.\n" +
                        "\n" +
                        "Осыған жауап ретінде, Астана IT University KPMG қолдауымен түрлі салалардағы күрделі жобаларды басқаратын озық дағдылары бар жобалардың басшыларын дайындау.");
                sendMessage.setChatId(message.getChatId());

            } else if (data.equals("mdEnglProg")) {
                sendMessage.setText("At our University, the only master's degree program is:Project management.\n" +
                        "Project management is a discipline with an ever-growing demand in both the private and public sector. Deutsche Bank Research says that by 2020, project economic activity will account for 15% of value creation in Germany. On the other hand, the \"Global project management report\" published in 2012 by Price Water House indicated that over the period 2000-2020, more than 25% of the economic value will come from projects. Therefore, the market will be in high demand for a qualified specialist specializing in project management.\n" +
                        "\n" +
                        "In response to this requirement, Astana IT University, with the support of KPMG, will train project managers with advanced skills who will manage complex projects in various industries.");
            } else if (data.equals("bRusAdmissions")) {

            } else if (data.equals("bKazAdmissions")) {

            } else if (data.equals("bEnglAdmissions")) {

            } else if (data.equals("mRusAdmissions")) {

            } else if (data.equals("mKazAdmissions")) {

            } else if (data.equals("mEnglAdmissions")) {

            }
            sendMessage.setChatId(message.getChatId());
            try {
                execute(sendMessage);
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }
        } else {
            Message message = update.getMessage();

            Integer messageId = message.getMessageId();

            String text = message.getText();
            User user = message.getFrom();
            System.out.println("messageId:" + messageId + "User_Name" + user.getFirstName() + "User_username" + user.getUserName() + "message:" + text);
            sendMessage.setChatId(message.getChatId());

            if (text.equals("/start") || text.equals("/help") || text.equals("help")) {

                this.sendMsg(this.commandsController.commands(text, message.getChatId(), messageId));

            } else if (text.equals("I'm an applicant") || text.equals("I am an applicant")
                    || text.equals("Bachelor's degree") || text.equals("Master's degree")
                    || text.equals("Tour") || text.equals("3D tour") || text.equals("See the University")
                    || text.equals("Contact")) {

                this.sendMsg(this.englishController.englDegrees(text, message.getChatId(), messageId));

            } else if (text.equals("Я абитуриент") || text.equals("Бакалавриат") || text.equals("Магистратура")
                    || text.equals("Сотрудничество с другими компаниями") || text.equals("Сотрудничество")
                    || text.equals("Связаться") || text.equals("3D тур") || text.equals("Увидеть университет")
                    || text.equals("Тур.") || text.equals("Вопрос")) {

                this.sendMsg(this.russianController.rusDegrees(text, message.getChatId(), messageId));

            } else if (text.equals("Мен абитуриентпін") || text.equals("Бакалаврлық") || text.equals("Магистрлық")
                    || text.equals("Басқа компаниялармен ынтымақтастық") || text.equals("Ынтымақтастық")
                    || text.equals("Тур") || text.equals("үш D тур") || text.equals("Университетті көру")
                    || text.equals("Байланысу")) {

                this.sendMsg(this.kazakhController.kazDegrees(text, message.getChatId(), messageId));

            } else if (text.equals("I'm a student") || text.equals("I am a student")) {

                this.sendMsg(this.englishController.englStudents(text, message.getChatId(), messageId));

            } else if (text.equals("Я студент")) {

                this.sendMsg(this.russianController.rusStudents(text, message.getChatId(), messageId));

            } else if (text.equals("Мен студентпін")) {

                this.sendMsg(this.kazakhController.kazStudents(text, message.getChatId(), messageId));

            } else if (text.equals("Hello") || text.equals("Hi") || text.equals("Привет") || text.equals("Салем")) {

                sendMessage.setText("Hey");

                this.sendMsg(sendMessage);

            } else {
                sendMessage.setChatId(update.getMessage().getChatId());

                try {
                    Question question = commandService.getAnswerByQuestion(message.getText());
                    sendMessage.setText(question.toString());
                    /*Question question1 = commandService.getAnswerByQuestionKaz(update.getMessage().getText());
                    sendMessage.setText(question1.toString());*/
                    try {
                        execute(sendMessage);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }

                } catch (Exception e) {
                    sendMessage.setText("Any more questions?");
                }
            }
        }
    }

    public void sendMsg(SendMessage sendMessage) {
        try {
            execute(sendMessage);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getBotUsername() {
        return USERNAME;
    }

    @Override
    public String getBotToken() {
        return TOKEN;
    }


}
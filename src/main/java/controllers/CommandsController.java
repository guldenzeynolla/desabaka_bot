package controllers;

import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import utill.InlineButtonUtill;

import java.util.ArrayList;
import java.util.List;

public class CommandsController {
    SendMessage sendMessage = new SendMessage();

    public SendMessage commands(String text, Long chatId, Integer messageId) {
        sendMessage.setChatId(chatId);

        if (text.equals("/start")) {

            sendMessage.setText("<b>Welcome</b> to the official assistant bot of Astana It University!\n" +
                    "Please, choose your preferred language");
            sendMessage.setParseMode("HTML");

            sendMessage.setReplyMarkup(InlineButtonUtill.keyboardMarkup(
                    InlineButtonUtill.collection(
                            InlineButtonUtill.rows(InlineButtonUtill.button("English", "english"),
                                    InlineButtonUtill.button("Русский", "russian")),
                            InlineButtonUtill.rows(InlineButtonUtill.button("Қазақша", "kazakh"))
                    )));
        } else if (text.equals("/help")) {
            String msg = "If you want to ask questions about developing a bot \n" +
                    "write to [Gulden Zeynolla] (http://wa.me/77081000000)\n" +

                    "If you want to ask questions about the admissions Committee,\n" +
                    " call +7 (7172)645-710 or email us info@astanait.edu.\n" +

                    "If you want to take part in the entrance exam in English language\n" +
                    " Astana IT University English Test,you can call us by numbers\n" +
                    " +7 717 267 7105, +7 778 818 5855 or write to e-mail info@astanait.edu.kz.\n" +
                    "If you still have any questions, please send us a \"help\"\n" +
                    " and we will get back to you as soon as possible.";
            sendMessage.setText(msg);
            sendMessage.setParseMode("Markdown");
            sendMessage = sendMessage.disableWebPagePreview();

        } else if (text.equals("help")) {
            sendMessage.setText("Give us your number and we will contact you");
            this.setRequestContact();
        }
        return sendMessage;

    }

    public  ReplyKeyboard setRequestContact() {
        ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();
        keyboardMarkup.setResizeKeyboard(true);
        keyboardMarkup.setOneTimeKeyboard(true);
        keyboardMarkup.setSelective(true);
        List<KeyboardRow> keyboard = new ArrayList<>();
        KeyboardRow row = new KeyboardRow();
        KeyboardButton button = new KeyboardButton();
        button.setText("Give the bot your number?");
        button.setRequestContact(true);
        row.add(button);
        keyboard.add(row);
        keyboardMarkup.setKeyboard(keyboard);
        return keyboardMarkup;
    }
}

package services;

import domain.EnglTestAppliciant;
import domain.Question;
import repositories.CategoryRepository;
import repositories.EnglTestRepository;
import repositories.QuestionRepository;
import repositories.interfaces.ICategoryRepository;
import repositories.interfaces.IEnglTestRepository;
import repositories.interfaces.IQuestionRepository;
import services.interfaces.ICommandService;

public class CommandService implements ICommandService {

    private IQuestionRepository questionRepository = new QuestionRepository();
    private ICategoryRepository categoryRepository = new CategoryRepository();
    private IEnglTestRepository englTestRepository = new EnglTestRepository();

    public void add(EnglTestAppliciant englTestAppliciant) {
        englTestRepository.add(englTestAppliciant);
    }

    public void update(EnglTestAppliciant englTestAppliciant) {
        englTestRepository.update(englTestAppliciant);
    }

    public void delete(EnglTestAppliciant englTestAppliciant) {
        englTestRepository.delete(englTestAppliciant);
    }

    public Question getAnswerByQuestion(String question_rus) {
        return questionRepository.getAnswerByQuestion(question_rus);
    }

    public Question getAnswerByQuestionKaz(String question_kaz) {
        return questionRepository.getAnswerByQuestionKaz(question_kaz);
    }


}

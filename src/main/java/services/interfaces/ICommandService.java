package services.interfaces;

import domain.EnglTestAppliciant;
import domain.Question;

public interface ICommandService {
    Question getAnswerByQuestion(String question_rus);
    Question getAnswerByQuestionKaz(String question_kaz);
    void add(EnglTestAppliciant englTestAppliciant);
    void update(EnglTestAppliciant englTestAppliciant);
    void delete(EnglTestAppliciant englTestAppliciant);
}

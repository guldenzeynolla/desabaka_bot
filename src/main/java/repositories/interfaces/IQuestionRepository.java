package repositories.interfaces;

import domain.Question;

public interface IQuestionRepository extends IEntityRepository<Question> {

    Question getAnswerByQuestion(String question_rus);
    Question getAnswerByQuestionKaz(String question_kaz);
}

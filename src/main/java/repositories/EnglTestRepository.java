package repositories;

import domain.EnglTestAppliciant;
import repositories.interfaces.IDBRepository;
import repositories.interfaces.IEnglTestRepository;

import javax.ws.rs.BadRequestException;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class EnglTestRepository implements IEnglTestRepository {
    private IDBRepository idbRepository = new PostgresRepository();


    @Override
    public void add(EnglTestAppliciant englTestAppliciant) {
        try {
            String sql = "INSERT INTO EnglAppliciants(email address, name, surname, IIN, phone, day) " +
                    "VALUES(?, ?, ?, ?, ?, ?)";
            PreparedStatement stmt = idbRepository.getConnection().prepareStatement(sql);
            stmt.setString(1, englTestAppliciant.getEmail());
            stmt.setString(2, englTestAppliciant.getName());
            stmt.setString(3, englTestAppliciant.getSurname());
            stmt.setLong(4, englTestAppliciant.getIIN());
            stmt.setLong(5, englTestAppliciant.getPhone());
            stmt.setDate(6, (Date) englTestAppliciant.getDate());

            stmt.executeUpdate();
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
    }

    @Override
    public void update(EnglTestAppliciant englTestAppliciant) {
        try {
            PreparedStatement stmt = idbRepository.getConnection().prepareStatement("UPDATE EnglAppliciants SET email address = ?, name = ?, surname = ?, phone = ?, day = ? WHERE IIN=?");
            stmt.setString(1, englTestAppliciant.getEmail());
            stmt.setString(2, englTestAppliciant.getName());
            stmt.setString(3, englTestAppliciant.getSurname());
            stmt.setLong(4, englTestAppliciant.getIIN());
            stmt.setLong(5, englTestAppliciant.getPhone());
            stmt.setDate(6, (Date) englTestAppliciant.getDate());

            stmt.executeUpdate();
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }

    @Override
    public void delete(EnglTestAppliciant englTestAppliciant) {
        PreparedStatement stmt = null;
        try {
            String sql = "DELETE FROM EnglAppliciants WHERE IIN=?";
            stmt = idbRepository.getConnection().prepareStatement(sql);
            stmt.setString(1, "IIN");
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


}


package repositories;

import domain.Question;
import repositories.interfaces.IDBRepository;
import repositories.interfaces.IQuestionRepository;

import javax.ws.rs.BadRequestException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class QuestionRepository implements IQuestionRepository {

    private  IDBRepository idbRepository = new PostgresRepository();


    public Question getAnswerByQuestion(String question_rus) {
        try {
            String sql = "SELECT * FROM questions WHERE question_rus = ?";
            PreparedStatement stmt = idbRepository.getConnection().prepareStatement(sql);
            stmt.setString(1, question_rus);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return new Question(
                        rs.getString("question_rus"),
                        rs.getString("answer_rus")
                );
            }
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
        return null;
    }

    public Question getAnswerByQuestionKaz(String question_kaz) {
        try {
            String sql = "SELECT * FROM questions WHERE question_kaz = ?";
            PreparedStatement stmt = idbRepository.getConnection().prepareStatement(sql);
            stmt.setString(1, question_kaz);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return new Question(
                        rs.getLong("id"),
                        rs.getString("question_kaz"),
                        rs.getString("answer_kaz")
                );
            }
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
        return null;
    }

    @Override
    public void add(Question entity) {

    }

    @Override
    public void update(Question entity) {

    }

    @Override
    public void delete(Question entity) {

    }


}


